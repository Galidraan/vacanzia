#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct{
	char emetteur[30];
	char recepteur[30];
	char sujet[30];
	char corps[666];
}mail;

enregistrer_mail(mail *mail_a_enregistrer);

int main()
{
    mail mailR[1000];   //Demander à l'utilisateur de rentrer
    enregistrer_mail(mailR);

}

enregistrer_mail(mail *mail_a_enregistrer)
{
    FILE * file;
    file = fopen("mails.txt", "a");     //Ouvre le fichier en mode écriture
	if(file == NULL)        //Si il y a un problème afficher "ERREUR"
	{
		printf("ERREUR");
		exit(1);
	}

	gets(mail_a_enregistrer);
	fprintf(file,"\n%s", mail_a_enregistrer);      //Ecrire dans le fichier
	fclose(file);       //Fermer le fichier
}
